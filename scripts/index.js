(function(w) {
  var behind = document.querySelector(".behind");
  var hero = document.querySelector(".hero");
  var name = document.querySelector(".name");
  var twitterIcon = document.querySelector(".twitter-icon");
  var twitterHandle = document.querySelector(".twitter-handle");
  var easterEggsMsg = document.querySelector(".easter-eggs-message");
  var easterEggsText = "There are $$ easter ** left";
  var twitterHandleText = "@jakecodes";
  var lastKonami = 0;
  var konamiSoFar = ""
  var isMouseDown = false;
  var totalEasterEggs = 5;
  var foundHighlight = false;
  var foundBird = false;
  var foundLove = false;
  var foundKonami = false;
  var foundJeanValjean = false;
  var foundHandsomeMan = false;

  w.addEventListener("scroll", function() {
    var percent = (w.scrollY / hero.offsetHeight) * 1.3;
    behind.style.opacity = percent;
  });

  function eggsOrEgg() {
    if (totalEasterEggs === 1) {
      return "egg";
    } else {
      return "eggs";
    }
  }
  
  function foundEgg() {
    totalEasterEggs--;
    easterEggsMsg.textContent = easterEggsText
      .replace("$$", totalEasterEggs)
      .replace("**", eggsOrEgg());
  }
  
  function handsome(fullText) {
    if(fullText.toLowerCase().startsWith('#1handsomeman')) {
      document.querySelector('.profile').classList.add('active');
      if(!foundHandsomeMan) {
        foundHandsomeMan = true;
        foundEgg();
      }
    } else {
      document.querySelector('.profile').classList.remove('active');
    }
  }

  function getSelectedText(e) {
    if (e.type === "mousedown") {
      isMouseDown = true;
    } else if (e.type === "mouseup") {
      isMouseDown = false;
    }
    var letters = w
      .getSelection()
      .toString()
      .split(" ")
      .join("");
    var lettersLength = letters.length;
    if (lettersLength === 0) {
      twitterHandle.textContent = "";
      return;
    }
    var letterOne = w.getSelection().toString().charAt(0).toLowerCase();
    if(letterOne !== 'j' && letterOne !== '#') return;
    if(letterOne === '#') {
      handsome(letters);
      return;
    }
    twitterHandle.textContent =
      "@" + twitterHandleText.slice(1, lettersLength + 1);
    if (!foundHighlight) {
      foundHighlight = true;
      foundEgg();
    }
  }

  function tweetDoubleClicked() {
    if(!foundBird) {
      foundBird = true;
      foundEgg();
    }
  }
  
  function clickedLove() {
    document.querySelector('.more-hearts-please').classList.add('active');
    if(!foundLove) {
      foundLove = true;
      foundEgg();
    }
  }
  
  function konami(e) {
    var now = Date.now();
    var difference = Date.now() - lastKonami;
    if(difference > 500) {
      konamiSoFar = "";
    }
    konamiSoFar += e.keyCode.toString();
    lastKonami = now;
    if(konamiSoFar === '38384040373937396665') {
      document.querySelector('body').style.transform = 'rotate3d(0,1,0,180deg)';
      setTimeout(function() {
        document.querySelector('body').removeAttribute('style');
      }, 2000);
      if(!foundKonami) {
        foundKonami = true;
        foundEgg();
      }
    }
  }
  
  function nameFieldCheck(e) {
    if(e.target.classList.contains('secret-name-field')) {
      if(e.target.value === "24601") {
        name.textContent = "Winner";
        w.scrollTo(0, 0);
        e.target.value = "";
        if(!foundJeanValjean) {
          foundJeanValjean = true;
          foundEgg();
        }
      }
    }
  }
  
  w.addEventListener("keyup", nameFieldCheck);
  w.addEventListener("keyup", konami);
  w.addEventListener("keyup", getSelectedText);
  w.addEventListener("touchstart", getSelectedText);
  w.addEventListener("touchend", getSelectedText);
  w.addEventListener("touchmove", getSelectedText);
  w.addEventListener("mouseup", getSelectedText);
  w.addEventListener("mousedown", getSelectedText);
  w.addEventListener("mousemove", getSelectedText);
  twitterHandle.addEventListener("dblclick", tweetDoubleClicked);
  document.querySelector('.made-with-love').addEventListener('click', clickedLove);
})(window);
