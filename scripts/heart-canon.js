(function(w) {
  var hearts = [];
  var started = false;
  var canvas = document.getElementById("heart-canon");
  var ctx = canvas.getContext("2d");
  var interval;
  var toRemove = [];
  var max = 300;
  var min = 40;
  var vyMax = 5;
  var vyMin = 1;
  var startedEver = false;
  var heartCanonTimer;
  function createHeart(x, y, r, color) {
    var s = w.shapes.create("heart-canon");
    s.setShadow("rgba(0,0,0,0.5", 10, 10, 20);
    s.setFill(color.r, color.g, color.b);
    s.fillHeart(x, y, 75, 75, r);
  }

  function redraw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    hearts.forEach(function(h) {
      if (!h.alive) return;
      h.x += h.vx;
      h.y += h.vy;
      h.r += h.vr;
      createHeart(h.x, h.y, h.r, h.color);
      if (h.y > 1200) {
        h.alive = false;
        toRemove.push(h);
      }
    });
    toRemove.forEach(function(h) {
      hearts.splice(hearts.indexOf(h), 1);
    });
    toRemove = [];
  }

  function heartWind() {
    hearts.forEach(function(h) {
      if (!h.alive) return;
      h.vx += (Math.random() >= 0.5 ? -1.5 : 1.5);
      createHeart(h.x, h.y, h.r, h.color);
      if (h.y > 1200) {
        h.alive = false;
        toRemove.push(h);
      }
    });
  }

  function heartAttack() {
    hearts.forEach(function(h) {
      if (!h.alive) return;
      var angle = 45;
      h.vy *= (Math.random() >= 0.5 ? -1 : 1);
      createHeart(h.x, h.y, h.r, h.color);
      if (h.y > 1200 || h.y < 0) {
        h.alive = false;
        toRemove.push(h);
      }
    });
  }

  function moreHearts(numHearts) {
    numHearts = numHearts || 5;
    if(hearts.length > 250) {
      document.querySelector('.too-many-hearts').classList.add('active');
      return;
    } else {
      document.querySelector('.too-many-hearts').classList.remove('active');
    }
    for (var i = 0; i < numHearts; i++) {
      var color = {
        r: Math.random() * 255,
        g: 0,
        b: Math.random() * 255
      };
      hearts.push({
        x: Math.random() * (max - min) + min,
        y: -1,
        vx: 0,
        vy: Math.random() * (vyMax - vyMin) + vyMin,
        r: 0,
        vr: Math.random() * 0.05,
        alive: true,
        color: color
      });
    }
  }
  
  function pause() {
    clearInterval(interval);
    clearInterval(heartCanonTimer);
  }

  function start() {
    startedEver = true;
    if (interval) {
      clearInterval(interval);
    }
    interval = setInterval(redraw, 1000 / 60);
    heartCanonTimer = setInterval(moreHearts, 1000);
    moreHearts();
  }
  
  document.querySelector('.made-with-love').addEventListener('click', function() {
    if(started){
      pause();
    } else {
      start();
    }
    started = !started;
  });
  document.querySelector('.little-hearts').addEventListener('click', function(e) {
    moreHearts(10);
  });
  document.querySelector('.little-more-hearts').addEventListener('click', function(e) {
    moreHearts(20);
  });
  document.querySelector('.lots-of-hearts').addEventListener('click', function(e) {
    moreHearts(30);
  });
  document.querySelector('.heart-wind').addEventListener('click', function(e) {
    heartWind();
  });
  document.querySelector('.heart-attack').addEventListener('click', function(e) {
    heartAttack();
  });
  w.addEventListener('blur', function() {
    if(startedEver) {
      pause(); 
    }
  });
  w.addEventListener('focus', function() {
    if(startedEver) {
      start();
    }
  });
}(window));
